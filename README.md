## CONTENTS OF THIS FILE ##

 * Introduction
 * Installation
 * Usage
 * How Can You Contribute?
 * Maintainers

## INTRODUCTION ##

Author and maintainer: Pawel Ginalski (gbyte)
 * Drupal: https://www.drupal.org/u/gbyte
 * Homepage: https://gbyte.dev/

This module allows for programmatic creating and sending of emails. It exposes a
OOP and twig template based API replacing the messy hook based approach used by
Drupal core. This uses Drupal services in the background and does not have any
additional dependencies.

This is a pure API module and is to be used programmatically (in other modules).

## INSTALLATION ##

See https://www.drupal.org/docs/extending-drupal/installing-modules for
instructions on how to install or update Drupal modules.

## USAGE ##

### Using the manager with custom email class & email template

This is the most common use case. The email class is used to define email
properties and the email template is used to define the email body.

1. Implement `Drupal\my_module\TestEmail` extending `Drupal\mail_composer\Email`:
    
    ```php
   <?php
   
   namespace Drupal\my_module;
   
   use Drupal\mail_composer\Email;
   
   class TestEmail extends Email {
   
     /**
      * @inheritDoc
      */
     public function getSubject(): string {
       return $this->t('My test email');
     }
   
     /**
      * @inheritDoc
      */
     public function getFrom(): string {
       return 'foobar@foo.bar';
     }
   
   }
    ```
   
   Other methods can be overridden as needed.

2. Implement `my_module/templates/emails/test-email.html.twig`:
    
    ```twig
   This is the body of the email with variable {{ my_variable_1 }} and
   {{ my_variable_2 }}.
    ```

   HTML templates can be used instead when using a module like swiftmailer.

3. Send the email:

    ```php
   /** @var \Drupal\mail_composer\Manager $manager */
   $manager = \Drupal::service('mail_composer.manager');

   $variables = ['my_variable_1' => 'foo', 'my_variable_2' => 'bar'];
   $email = new \Drupal\my_module\TestEmail($variables);
   $manager->compose($email)->setTo('foo@bar.bar')->send();
   ```
   All email settings defined in the TestEmail class (optional) can be
   overridden with the manager's setters.

### Using the manager with custom email class without email template

If you don't care for graceful formatting and translating of the email body, you
can skip the template (step 2 above) and either add `TestEmail::getBody()` or
use `$manager::setBody` (see below).

### Using the manager with default email class

By using `$manager->compose()` without providing the custom email class as
argument, the default email class is used (see `Drupal\mail_composer\Email`).
Things like to, from, subject and body need to be set manually in the manager:

```php
/** @var \Drupal\mail_composer\Manager $manager */
$manager = \Drupal::service('mail_composer.manager');

$manager->setSubject('My test email')
->compose()
->setFrom('foobar@foo.bar')
->setTo('foo@bar.bar')
->setSubject('Test subject')
->setBody(['This is the body of the email.'])
->send();
```

### Using the manager without email class

Just do not call `$manager->compose()` and set all email settings manually
before `$manager->send()`. This is not recommended.

### MULTILINGUAL EMAILS ###

- The email subject can be translated using `t()`.
- The email language can be manually set via `$manager->setLangcode()` or in the
  `TestEmail` class.
- It is possible to have per-language email body templates by adding a suffix 
  for non-default languages, e.g. `test-email.de.html.twig`.
- When not using email body templates, `t()` can be used to translate the body.

## HOW CAN YOU CONTRIBUTE? ##

 * Report any bugs, feature or support requests in the issue tracker; if
   possible help out by submitting patches.
   http://drupal.org/project/issues/mail_composer

 * If you would like to say thanks and support the development of this module, a
   donation will be much appreciated.
   https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=5AFYRSBLGSC3W

 * Feel free to contact me for paid support: https://gbyte.dev/contact

## MAINTAINERS ##

Current maintainers:
 * Pawel Ginalski (gbyte) - https://www.drupal.org/u/gbyte
