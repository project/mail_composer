<?php

namespace Drupal\mail_composer;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\mail_composer\Exception\MailingException;

/**
 * Class EmailManager
 * @package Drupal\mail_composer\Email
 */
class Manager {

  protected const EMAIL_PROPERTIES = ['body', 'subject', 'from', 'to', 'replyTo', 'langcode', 'key', 'headers'];

  protected const REQUIRED_EMAIL_PROPERTIES = ['to', 'langcode', 'key'];

  /**
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected MailManagerInterface $mailManager;

  /**
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected LanguageManager $languageManager;

  /**
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected ConfigFactory $configFactory;

  /**
   * @var string|null
   */
  protected ?string $from;

  /**
   * @var string|null
   */
  protected ?string $to;

  /**
   * @var string|null
   */
  protected ?string $subject;

  /**
   * @var string[]|null
   */
  protected ?array $body;

  /**
   * @var string|null
   */
  protected ?string $key;

  /**
   * @var string|null
   */
  protected ?string $langcode;

  /**
   * @var string|null
   */
  protected ?string $replyTo;

  /**
   * @var array|null
   */
  protected ?array $headers;

  /**
   * EmailManager constructor.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   */
  public function __construct(
    MailManagerInterface $mail_manager,
    LanguageManager $language_manager,
    ConfigFactory $config_factory
  ) {
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * @param string|null $from
   *
   * @return $this
   */
  public function setFrom(?string $from): Manager {
    $this->from = $from;
    return $this;
  }

  /**
   * @param string|null $to
   *
   * @return $this
   */
  public function setTo(?string $to): Manager {
    $this->to = $to;
    return $this;
  }

  /**
   * @param string|null $subject
   *
   * @return $this
   */
  public function setSubject(?string $subject): Manager {
    $this->subject = $subject;
    return $this;
  }

  /**
   * @param array|null $body
   *  Array of body paragraphs.
   *
   * @return $this
   */
  public function setBody(?array $body): Manager {
    $this->body = $body;
    return $this;
  }

  /**
   * @param string|null $email
   *
   * @return $this
   */
  public function setReplyTo(?string $email): Manager {
    $this->replyTo = $email;
    return $this;
  }

  /**
   * Define the Drupal email key without this module's name.
   *
   * If not defined, mail_composer will be used.
   *
   * @param string|null $key
   *
   * @return $this
   */
  public function setKey(?string $key): Manager {
    $this->key = $key;
    return $this;
  }

  /**
   * @param string|null $langcode
   *
   * @return $this
   */
  public function setLangcode(?string $langcode): Manager {
    $this->langcode = $langcode;
    return $this;
  }

  /**
   * @param array|null $headers
   *
   * @return $this
   */
  public function setHeaders(?array $headers): Manager {
    $this->headers = $headers;
    return $this;
  }

  /**
   * @return bool
   *
   * @throws \Drupal\mail_composer\Exception\MailingException
   */
  public function send(): bool {
    foreach (self::REQUIRED_EMAIL_PROPERTIES as $property) {
      if (empty($this->$property)) {
        throw new MailingException("Email property '$property' cannot be empty.");
      }
    }

    $result = $this->mailManager->mail(
      'mail_composer',
      $this->key,
      $this->to,
      $this->langcode, [
        'from' => (string) $this->from,
        'subject' => (string) $this->subject,
        'body' => $this->body ?? [],
        'headers' => $this->headers ?? [],
      ],
      $this->replyTo
    );

    return $result['result'];
  }

  /**
   * @param \Drupal\mail_composer\EmailInterface|null $email
   *
   * @return $this
   */
  public function compose(?EmailInterface $email = NULL): Manager {
    $email = $email ?? new Email();
    foreach (self::EMAIL_PROPERTIES as $property) {
      $this->$property = NULL;
      $this->{'set' . ucfirst($property)}($email->{'get' . ucfirst($property)}());
    }

    return $this;
  }
}
