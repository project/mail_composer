<?php

namespace Drupal\mail_composer;

/**
 * Interface EmailInterface
 */
interface EmailInterface {

  /**
   * @return string
   */
  public function getSubject(): string;

  /**
   * array $body
   */
  public function getBody(): array;

  /**
   * @return string
   */
  public function getFrom(): string;

  /**
   * @return string
   */
  public function getTo(): string;

  /**
   * @return string|null
   */
  public function getReplyTo(): ?string;

  /**
   * @return string
   */
  public function getKey(): string;

  /**
   * @return string
   */
  public function getLangcode(): string;

  /**
   * @return array|null
   */
  public function getHeaders(): ?array;
}
