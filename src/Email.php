<?php

namespace Drupal\mail_composer;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Twig\Error\LoaderError;

/**
 * Class Email
 */
class Email implements EmailInterface {

  use StringTranslationTrait;

  /**
   * @var array
   */
  protected array $variables;

  /**
   * Email constructor.
   *
   * @param array $variables
   */
  public function __construct(array $variables = []) {
    $this->variables = $variables;
  }

  public function getFrom(): string {
    return (string) \Drupal::configFactory()->get('system.site')->get('mail');
  }

  public function getTo(): string {
    return '';
  }

  public function getReplyTo(): ?string {
    return NULL;
  }

  public function getSubject(): string {
    return '';
  }

  /**
   * Tries to load the email template corresponding to this class' name and the
   * current language.
   *
   * If templates/emails/some-email.xx.twig.html is not found,
   * some-email.twig.html will be used. If no template is found,an empty array
   * will be returned.
   *
   * @return array
   *
   * @throws \Throwable
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   */
  public function getBody(): array {
    include_once \Drupal::root() . '/core/themes/engines/twig/twig.engine';

    /** @var \Twig\Environment $twig_service */
    $twig_service = \Drupal::service('twig');

    foreach ([$this->getLangcode(), NULL] as $langcode) {
      if ($template_path = $this->getTemplatePath($langcode)) {
        try {
          $twig_service->load($template_path);
        }
        catch (LoaderError $ex) {
          continue;
        }

        // As this module uses twig templates in a non-standard way, this works
        // around 'Warning: Undefined array key "theme_hook_original" in
        // twig_render_template()'. An alternative would be to directly render
        // the templates, but then we loose the ability for our templates to be
        // altered by other modules, e.g. in case of HTML emails.
        if (!isset($this->variables['theme_hook_original'])) {
          $this->variables['theme_hook_original'] = '';
        }

        return [twig_render_template($template_path, $this->variables)];
      }
    }

    return [];
  }

  /**
   * Defines the template file path location relative to Drupal root.
   *
   * Default implementation:
   * If this email class is called SomeEmail, the template must be located
   * in the templates/emails folder inside the implementing module and be
   * called some-email.twig.html (for the default language), or
   * some-email-xx.twig.html for a specific language.
   *
   * @param string|null $langcode
   *
   * @return string|null
   *  The template file path relative to Drupal root, or NULL if no template is
   *  to be used.
   */
  protected function getTemplatePath(?string $langcode = NULL): ?string {
    $reflection_class = new \ReflectionClass($this);
    if (($namespace = $reflection_class->getName())
      && count($parts = explode('\\', $namespace)) > 1
      && $parts[0] === 'Drupal'
    ) {
      /** @var ModuleExtensionList $module_extension_list */
      $module_extension_list = \Drupal::service('extension.list.module');

      $file_name = strtolower(preg_replace('/(?<!^)[A-Z]/', '-$0', $reflection_class->getShortName()));
      $langcode_suffix = $langcode ? '.' . $langcode : '';

      return $module_extension_list->getPath($parts[1])
        . '/templates/emails/' . $file_name . $langcode_suffix . '.html.twig';
    }

    return NULL;
  }

  /**
   * Takes the current email template's class name and converts it to a Drupal
   * email key.
   *
   * Example: 'SomeEmail' will be converted to 'mail_composer_some_email'.
   *
   * @return string
   */
  public function getKey(): string {
    return 'mail_composer_' . strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', (new \ReflectionClass($this))->getShortName()));
  }

  public function getLangcode(): string {
    return \Drupal::languageManager()->getCurrentLanguage()->getId();
  }

  public function getHeaders(): array {
    return [];
  }

}
